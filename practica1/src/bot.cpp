#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
//Libreria mmap
#include <sys/mman.h>

#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"

int main(){

	/*Declaración de variables*/
	DatosMemCompartida *pmemoc;
	int fichero;
	char *proy;

	/*Apertura fichero*/
	fichero=open("bot.txt",O_RDWR);

	/*Proyección en memoria*/
	
	proy=(char*)mmap(NULL,sizeof(pmemoc),PROT_WRITE|PROT_READ,MAP_SHARED,fichero,0);
	
	/*Cierre del fd del fichero*/
	close(fichero);

	/*Asignación dirección de comienzo de la región*/
	pmemoc=(DatosMemCompartida*)proy;

	/*Bucle*/
	while(1){
		usleep(25000);
		float posr1;
		posr1=0.5*((pmemoc->raqueta1.y1)+(pmemoc->raqueta1.y2));
		if (posr1< pmemoc->esfera.centro.y)
			pmemoc->accion=1;
		else if (posr1> pmemoc->esfera.centro.y)
			pmemoc->accion=-1;
		else
			pmemoc->accion=0;
	}
	
	/*Desproyección*/
	munmap(proy,sizeof(*(pmemoc)));
	
}

	
